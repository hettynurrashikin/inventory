# README
API documentation https://documenter.getpostman.com/view/4407956/RWEZQh3d

# Requirements
- Ruby 2.3.4
- Rails 5.1.4
- PostgreSQL 9.6

# Commands to set up database
- rails db:create
- rails db:schema:load
- 'rails db:seeds' to populate the database with contents