class CreateReservationItems < ActiveRecord::Migration[5.1]
  def change
    create_table :reservation_items do |t|
      t.string :requested_item
      t.integer :quantity_borrowed
      t.integer :quantity_returned,default: 0
      t.datetime :borrowed_at
      t.datetime :returned_at
      t.boolean :returned_flag, default: false

      t.references :reservation, foreign_key: true
      t.references :item, foreign_key: true
      t.timestamps
    end
  end
end
