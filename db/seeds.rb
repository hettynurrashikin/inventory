# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# To run seed.rb type rails db:seed

locations = Location.create([{name: "Storeroom"}, {name: "Recycling Corner"}, {name: "pantry"}])

storeroom_items = Location.find_by(name: "Storeroom").location_items.create([{name: "Arduino", quantity: 15}, {name: "Continuous Servos", quantity: 167}, {name: "AA batteries", quantity: 785}, {name:"micro:bit", quantity: 64},{name:"Jumper wires", quantity: 1028}, {name:"buzzer", quantity: 100}])
recycling_corner_items = Location.find_by(name: "Recycling Corner").location_items.create([{name: "Cardboard", quantity: 5, {name: "Water bottles", quantity: 6}}])
pantry_items = Location.find_by(name: "Pantry").location_items.create([{name: "Ice Lemon Tea", quantity: 12}, {name: "Jasmine Green Tea", quantity: 3}])


reservations = reservations.create([{name: "Microbit Class, ABC primary school", completed_flag = false}])
reservation_items = Reservation.first.reservation_items.create(requested_item:"micro:bit", quantity_borrowed: 24, borrowed_at: "24/05/2018", borrowed_at: "25/05/2018")
reservation_items = Reservation.first.reservation_items.create(requested_item:"buzzer", quantity_borrowed: 24, borrowed_at: "24/05/2018", borrowed_at: "25/05/2018")
reservation_items = Reservation.first.reservation_items.create(requested_item:"Jumper wires", quantity_borrowed: 48, borrowed_at: "24/05/2018", borrowed_at: "25/05/2018")
reservation_items = Reservation.first.reservation_items.create(requested_item:"AA batteries", quantity_borrowed: 60, borrowed_at: "24/05/2018", borrowed_at: "25/05/2018")
