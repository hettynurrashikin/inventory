json.extract! reservation, :id, :name, :completed_flag, :created_at, :updated_at
json.url reservation_url(reservation, format: :json)
