json.extract! reservation_item, :id, :requested_item, :quantity_borrowed, :quantity_returned, :borrowed_at, :returned_at, :datetime, :created_at, :updated_at
json.url reservation_item_url(reservation_item, format: :json)
