class Location < ApplicationRecord
  #has_many :location_items
  #has_many :items, through: :location_items
  has_many :items

  before_validation :save_location, :on => :create

  # Before destroying a location, transfer all items into storeroom

  private
  def save_location
    if Location.exists?(name: self.name)
      errors.add(:base, 'Cannot add location, location exists')

      #return false
    end
  end

end
