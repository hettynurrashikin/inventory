class ReservationItem < ApplicationRecord
  belongs_to :reservation

  before_create :chosenItem
  before_update :compareReturnQtyToBorrowedQty
  after_update_commit :checkReservationStatus


  def chosenItem
    item_id = self.requested_item
    self.requested_item = Item.find(item_id).name
    self.item_id = item_id
  end

  def compareReturnQtyToBorrowedQty

    # Dirty callbacks - Checks if user edited the return field
    if quantity_returned_changed?

      # Check if qty return = qty borrowed
      if self.quantity_returned == self.quantity_borrowed
        self.returned_flag = true   # Complete return, close case
        return true
      end

      # Check if qty return < qty borrowed
      if self.quantity_returned < self.quantity_borrowed
        self.returned_flag = false
        return true
      end

      if self.quantity_returned > self.quantity_borrowed
          return false
          #self.errors.add(:quantity_returned, "is invalid") if self.quantity_returned > self.quantity_borrowed
          flash[:error] = "Cannot return more than what was borrowed"
      end
      
    end
  end


  # Updates reservation's completed flag if borrower have returned all items
  def checkReservationStatus

    completed_count = 0

    reservation = Reservation.find(self.reservation_id)

    reservation.reservation_items.each do |ri|
        if ri.returned_flag == true
          completed_count += 1
        end
    end

    if completed_count == 0
      reservation.completed_flag = false
      reservation.save

    elsif completed_count != reservation.reservation_items.count
        reservation.completed_flag = false
        reservation.save

    elsif completed_count == reservation.reservation_items.count
        reservation.completed_flag = true
        reservation.save
    end
  end

end
