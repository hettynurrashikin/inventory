class Item < ApplicationRecord

  belongs_to :location

  #validates :name, :uniqueness => {:message => "Title already exists."}
  before_validation :save_item, :on => :create

  private
  def save_item
    if Item.exists?(name: self.name, location_id: self.location_id)
      errors.add(:name, 'Cannot add item, item exists')
    end
  end
end
