jQuery ->
  $('#item_id').parent().hide()
  items = $('#person_state_id').html()
  $('#location_id').change ->
    location = $('#location_id :selected').text()
    escaped_location = location.replace(/([ #;&,.+*~\':"!^$[\]()=>|\/@])/g, '\\$1')
    options = $(items).filter("optgroup[label='#{escaped_location}']").html()
    if options
      $('#item_id').html(options)
      $('#item_id').parent().show()
    else
      $('#item_id').empty()
      $('#item_id').parent().hide()
