module Api
  module V1
    class ReservationsController < ApplicationController

      protect_from_forgery with: :null_session

      #CRUD
      def index
        reservations = Reservation.all
        render json: {status: 'SUCCESS', message: 'Loaded', data: reservations}, status: :ok
      end

      def show

        reservation_items = Reservation.find(params[:id]).reservation_items
        render json:{status: 'SUCCESS', message: 'Showed Reservation Items', data: reservation_items}, status: :unprocessable_entity
      end

      def create
        reservation = Reservation.new(create_reservation_params)

        if reservation.save
          render json:{status: 'SUCCESS', message: 'Reservation Created', data: reservation}, status: :ok
        else
          render json:{status: 'ERROR', message: 'Reservation not created', data: reservation.errors}, status: :unprocessable_entity
        end
      end

      def update

        reservation = Reservation.find(params[:id])

        if reservation.update_attributes(update_reservation_params)
          render json: {status: 'SUCCESS', message: 'Reservation Updated', data: reservation}, status: :ok
        else
          render json: {status: 'ERROR', message: 'Reservation not updated', data: reservation.errors}, status: :ok
        end
      end

      def destroy
        reservation = Reservation.find(params[:id])

        if reservation.destroy
          render json: {status: 'SUCCESS', message: 'Reservation Deleted', data: reservation}, status: :ok
        else
          render json: {status: 'Error', message: 'Cannot delete reservation', data: reservation.errors}, status: :ok
        end
      end

      def create_reservation_params
        params.permit(:name)
      end

      def update_reservation_params
        params.permit(:name, :completed_flag)
      end

    end
  end
end
