module Api
  module V1
    class ItemsController < ApplicationController
      protect_from_forgery with: :null_session


      def create

        item = Item.new(item_params)

        if item.save
          render json:{status: "SUCCESS", message: "Saved Item", data: item}, status: :ok
        else
          render json:{status: "ERROR", message: "Item not saved", data: item.errors}, status: :unprocessable_entity
        end
      end


      def update
        location = Location.find(params[:location_id])
        item = location.items.find(params[:id])

        if item.update_attributes(update_item_params)
          render json:{status: "SUCCESS", message: "Item updated", data: item}, status: :ok
        else
          render json:{status: "ERROR", message: "Item not updated", data: item.errors}, status: :unprocessable_entity
        end
      end

      def destroy
        location = Location.find(params[:location_id])
        item = location.items.find(params[:id])

        if item.destroy
          render json:{status: "SUCCESS", message: "Item destroyed", data: item}, status: :ok
        else
          render json:{status: "ERROR", message: "Item cannot be destroyed", data: item.errors}, status: :unprocessable_entity
        end
      end


      def item_params
        params.permit(:name, :quantity, :location_id)
      end

      def update_item_params
        params.permit(:name, :quantity)
      end


    end
  end
end
