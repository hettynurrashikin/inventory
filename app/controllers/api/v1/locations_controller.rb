module Api
  module V1
    class LocationsController < ApplicationController
      protect_from_forgery with: :null_session

      def index
        locations = Location.all
        render json: {status: 'SUCCESS', message: 'Loaded Locations', data: locations}, status: :ok
      end

      def show
        location = Location.find(params[:id])
        items = location.items

        render json: {status: 'SUCCESS', message: 'Loaded Locations Items', data: items}, status: :ok
      end

      def create
        location = Location.new(location_params)

        if location.save
          render json: {status: 'SUCCESS', message: 'Saved Location', data: location}, status: :ok
        else
          render json: {status: 'ERROR', message: 'Location not saved', data: location.errors}, status: :unprocessable_entity
        end
      end

      def update
        location = Location.find(params[:id])

        if location.update_attributes(location_params)
          render json: {status: 'SUCCESS', message: 'Location Updated', data: location}, status: :ok
        else
          render json: {status: 'Error', message: 'Location not updated', data: location.errors}, status: :ok
        end
      end

      def destroy
        location = Location.find(params[:id])

        if location.destroy
          render json: {status: 'SUCCESS', message: 'Location Deleted', data: location}, status: :ok
        else
          render json: {status: 'Error', message: 'Cannot delete location', data: location.errors}, status: :ok
        end
      end

      def location_params
        params.permit(:name)
      end
    end

  end
end
