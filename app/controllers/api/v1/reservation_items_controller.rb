module Api
  module V1
    class ReservationItemsController < ApplicationController
      protect_from_forgery with: :null_session

      def create

        reservation = Reservation.find(params[:reservation_id])
        reservation_item = reservation.reservation_items.new(create_reservation_item_params)

        if reservation_item.save
          render json:{status: 'SUCCESS', message: 'Added item into reservation', data: reservation_item}, status: :ok
        else
          render json:{status: 'ERROR', message: ' Error in adding item into reservation', data: reservation_item.errors}, status: :unprocessable_entity
        end
      end

      def update
        reservation = Reservation.find(params[:reservation_id])
        reservation_item = reservation.reservation_items.find(params[:id])

        if reservation_item.update_attributes(update_reservation_item_params)
          render json: {status: 'SUCCESS', message: 'Reservation Updated', data: reservation_item}, status: :ok
        else
          render json: {status: 'ERROR', message: 'Reservation not updated', data: reservation_item.errors}, status: :ok
        end
      end

      def destroy
        reservation = Reservation.find(params[:reservation_id])
        reservation_item = reservation.reservation_items.find(params[:id])

        if reservation.destroy
          render json: {status: 'SUCCESS', message: 'Reservation Deleted', data: reservation_item}, status: :ok
        else
          render json: {status: 'Error', message: 'Cannot delete reservation', data: reservation_item.errors}, status: :ok
        end
      end


      def create_reservation_item_params
        params.permit(:requested_item, :quantity_borrowed, :borrowed_at, :returned_at)
      end

      def update_reservation_item_params
        params.permit(:requested_item, :quantity_returned, :borrowed_at, :returned_at)
      end

    end
  end
end
