class ReservationItemsController < ApplicationController
  before_action :set_reservation_item, only: [:show, :edit, :update, :destroy]


  # GET /reservation_items
  # GET /reservation_items.json
  def index
    @reservation_items = ReservationItem.all
  end

  # GET /reservation_items/1
  # GET /reservation_items/1.json
  def show
  end

  # GET /reservation_items/new
  def new
    @reservation = Reservation.find(params[:reservation_id])
    @reservation_item = @reservation.reservation_items.new

    @locations = Location.all
  end

  # GET /reservation_items/1/edit
  def edit
  end

  # POST /reservation_items
  # POST /reservation_items.json
  def create
    @reservation = Reservation.find(params[:reservation_id])
    @reservation_item = @reservation.reservation_items.new(reservation_item_params)

    respond_to do |format|
      if @reservation_item.save
        format.html { redirect_to reservation_path(@reservation.id), notice: 'Reservation item was successfully created.' }
        format.json { render :show, status: :created, location: @reservation_item }
      else
        format.html { render :new }
        format.json { render json: @reservation_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /reservation_items/1
  # PATCH/PUT /reservation_items/1.json
  def update
    respond_to do |format|

      if @reservation_item.update(reservation_item_params)

        if ReservationItem.compareReturnQtyToBorrowedQty == true
          format.html { redirect_to reservation_path(@reservation.id), notice: 'Reservation item was successfully updated.' }
          format.json { render :show, status: :ok, location: @reservation_item }

        else
          flash[:notice] = "Cannot return more than what was borrowed"
          render :edit
        end

      else

        format.html { render :edit }
        format.json { render json: @reservation_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /reservation_items/1
  # DELETE /reservation_items/1.json
  def destroy
    @reservation_item.destroy
    respond_to do |format|
      format.html { redirect_to reservation_items_url, notice: 'Reservation item was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_reservation_item
      @reservation = Reservation.find(params[:reservation_id])
      @reservation_item = @reservation.reservation_items.find(params[:id])
      #@reservation_item = ReservationItem.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def reservation_item_params
      params.require(:reservation_item).permit(:requested_item, :quantity_borrowed, :quantity_returned, :borrowed_at, :returned_at, :datetime)
    end
end
