Rails.application.routes.draw do


  #devise_for :users, :controllers => { registrations: 'registrations' }

  namespace :api, defaults:{ format: :json } do
    namespace :v1 do

      resources :locations, only: [:index, :show, :new, :create, :edit, :update, :destroy] do
        resources :items, only: [:new, :create, :edit, :update, :destroy]
      end

      resources :reservations, only: [:index, :show, :new, :create, :edit, :update, :destroy] do
        resources :reservation_items, only: [:new, :create, :edit, :update, :destroy]
      end

    end # v1
  end # api

  resources :locations, only: [:index, :show, :new, :create, :edit, :update, :destroy] do
    resources :items, only: [:new, :create, :edit, :update, :destroy]
  end

  resources :reservations, only: [:index, :show, :new, :create, :edit, :update] do
        resources :reservation_items, only: [:new, :create, :edit, :update]
  end

  root to: "locations#index"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

end
